
const findSumNumbers = function (lista, valor) {
    if (!lista) {
        throw new Error('Lista está indefinido!');
    }
    else if (lista.length === 0) {
        throw new Error('Lista está vacía!');
    }
    if (!valor) {
        throw new Error('Valor está indefinido!');
    }
    var auxLista = new Array();
    let i = 0;
    let found = false;
    while(i<lista.length && !found) {
        const n = lista[i];
        if(n < valor) {
            let j=(i+1);
            while(j<lista.length && !found) {
                const s = lista[j];
                if(s < valor && (n+s) === valor) {
                    auxLista.push(n);
                    auxLista.push(s);
                    found = true;
                }
                j++;
            }
        }
        i++;
    }
    if (auxLista.length < 2) {
        throw new Error(`No se encontraron valores en la lista que sumados den ${valor}`);
    }
    return auxLista;
};

const array = [9,2,3,5,7,8,14,0,1];
const N = 10;

const resultado = findSumNumbers(array, N);
console.log(`Valores que suman ${N}`, resultado);
