export class Item {
    name: string;
    sellIn: number;
    quality: number;

    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

const arrayProducts = ['Aged Brie', 'Backstage passes to a TAFKAL80ETC concert', 'Sulfuras, Hand of Ragnaros', 'Conjured'];

export class GildedRose {
    items: Array<Item>;

    constructor(items = [] as Array<Item>) {
        this.items = items;
    }

    updateQuality() {
        this.items.forEach(item => {
            let { name, quality, sellIn } = item;
            if(arrayProducts[0] !== name && arrayProducts[1] !== name) {
                if(quality > 0 && arrayProducts[2] !== name) {
                    quality = quality - 1;
                }
                if(arrayProducts[3] === name) {
                    quality = quality - 1;
                }
            } else {
                if(quality < 50) {
                    quality = quality + 1;
                    if(arrayProducts[1] === name) {
                        if(sellIn < 11) {
                            quality = quality + 1;
                        }
                        if(sellIn < 6) {
                            quality = quality + 1;
                        }
                    }
                }
            }
            if(arrayProducts[2] !== name) {
                sellIn = sellIn - 1;
            }
            if(sellIn < 0) {
                if(arrayProducts[0] !== name) {
                    if(arrayProducts[1] !== name) {
                        if(arrayProducts[2] !== name && quality > 0) {
                            quality = quality - 1;
                        }
                    } else {
                        quality = quality - quality;
                    }
                } else if(quality < 50) {
                    quality = quality + 1;
                }
            }
        });
        return this.items;
    }
}
